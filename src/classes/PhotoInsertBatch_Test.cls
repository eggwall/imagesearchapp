/**
 * Created by Dominika Wałęga on 21.08.2019.
 */

@isTest
private class PhotoInsertBatch_Test {
    @testSetup
    private static void testSetup() {
        Photo__c photo = new Photo__c();
        photo.Id_Number__c = 1;
        photo.Thumbnail_Url__c = 'http://photoHttpCalloutMockTest.com';
        photo.Title__c = 'PhotoHttpCalloutMockTest 1';
        photo.Url__c = 'http://photoHttpCalloutMockTest.com';
        insert photo;
    }

    @isTest
    private static void getPhotosObjects_Test() {
        System.assertEquals(1, [SELECT Id FROM Photo__c].size());

        Test.setMock(HttpCalloutMock.class, new PhotoHttpCalloutMock());

        Test.startTest();
        PhotoInsertBatch photoInsertBatch = new PhotoInsertBatch();
        Database.executeBatch(photoInsertBatch);
        Test.stopTest();

        System.assertEquals(3, [SELECT Id FROM Photo__c].size());
    }
}