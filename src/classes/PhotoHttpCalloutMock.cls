/**
 * Created by Dominika Wałęga on 23.08.2019.
 */

@isTest
public class PhotoHttpCalloutMock implements HttpCalloutMock {

    public static HTTPResponse respond(HTTPRequest req) {
        String body = '[' +
                '  {' +
                '    \"albumId\": 1,' +
                '    \"id\": 1,' +
                '    \"title\": \"PhotoHttpCalloutMockTest 1\",' +
                '    \"url\": \"http://photoHttpCalloutMockTest.com",' +
                '    \"thumbnailUrl\": \"http://photoHttpCalloutMockTest.com\"' +
                '  },' +
                '  {' +
                '    \"albumId\": 1,' +
                '    \"id\": 2,' +
                '    \"title\": \"PhotoHttpCalloutMockTest 2\",' +
                '    \"url\": \"http://photoHttpCalloutMockTest.com",' +
                '    \"thumbnailUrl\": \"http://photoHttpCalloutMockTest.com\"' +
                '  },' +
                '  {' +
                '    \"albumId\": 1,' +
                '    \"id\": 3,' +
                '    \"title\": \"PhotoHttpCalloutMockTest 3\",' +
                '    \"url\": \"http://photoHttpCalloutMockTest.com",' +
                '    \"thumbnailUrl\": \"http://photoHttpCalloutMockTest.com\"' +
                '  }' +
                ']';

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(body);
        res.setStatusCode(200);
        return res;
    }
}