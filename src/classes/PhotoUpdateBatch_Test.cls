/**
 * Created by Dominika Wałęga on 21.08.2019.
 */

@isTest
private class PhotoUpdateBatch_Test {
    @testSetup
    private static void testSetup() {
        List<Photo__c> photos = new List<Photo__c>();
        for (Integer i = 0; i < 100; i++) {
            Photo__c photo = new Photo__c();
            photo.Id_Number__c = i;
            photo.Thumbnail_Url__c = 'http://test.com';
            photo.Title__c = 'TestTitle' + i;
            photo.Url__c = 'http://test.com';

            photos.add(photo);
        }
        insert photos;
    }

    @isTest
    private static void getPhotosByTitle_Test() {
        List<Photo__c> photos = new List<Photo__c>();
        for (Integer i = 0; i < 50; i++) {
            Photo__c photo = new Photo__c();
            photo.Id_Number__c = i;
            photo.Thumbnail_Url__c = 'http://photoUpdateBatchTest.com';
            photo.Title__c = 'TestTitle' + i;
            photo.Url__c = 'http://test.com';

            photos.add(photo);
        }

        Test.startTest();
        PhotoUpdateBatch photoUpdateBatch = new PhotoUpdateBatch(photos);
        Database.executeBatch(photoUpdateBatch);
        Test.stopTest();

        System.assertEquals(50, [SELECT Id FROM Photo__c WHERE Thumbnail_Url__c = 'http://photoUpdateBatchTest.com'].size());
    }
}