/**
 * Created by Dominika Wałęga on 20.08.2019.
 */

global class PhotoSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
        PhotoInsertBatch photoInsertBatch = new PhotoInsertBatch();
        Database.executeBatch(photoInsertBatch);
    }
}
