/**
 * Created by Dominika Wałęga on 21.08.2019.
 */

@isTest
private class PhotoResponse_Test {

    @isTest
    private static void testParse() {
        String json = '[' +
                '  {' +
                '    \"albumId\": 1,' +
                '    \"id\": 1,' +
                '    \"title\": \"PhotoResponseTest 1\",' +
                '    \"url\": \"http://photoResponseTest.com\",' +
                '    \"thumbnailUrl\": \"http://photoResponseTest.com\"' +
                '  },' +
                '  {' +
                '    \"albumId\": 1,' +
                '    \"id\": 2,' +
                '    \"title\": \"PhotoResponseTest 2\",' +
                '    \"url\": \"http://photoResponseTest.com\",' +
                '    \"thumbnailUrl\": \"http://photoResponseTest.com\"' +
                '  },' +
                '  {' +
                '    \"albumId\": 1,' +
                '    \"id\": 3,' +
                '    \"title\": \"PhotoResponseTest 3\",' +
                '    \"url\": \"http://photoResponseTest.com\",' +
                '    \"thumbnailUrl\": \"http://photoResponseTest.com\"' +
                '  }' +
                ']';
        List<PhotoResponse> obj = PhotoResponse.parse(json);
        System.assert(obj != null);
    }
}
