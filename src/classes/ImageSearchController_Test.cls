/**
 * Created by Dominika Wałęga on 22.08.2019.
 */

@isTest
private class ImageSearchController_Test {
    @testSetup
    private static void testSetup() {
        List<Photo__c> photos = new List<Photo__c>();
        for (Integer i = 0; i < 100; i++) {
            Photo__c photo = new Photo__c();
            photo.Id_Number__c = i;
            photo.Thumbnail_Url__c = 'http://test.com';
            photo.Title__c = 'TestTitle' + i;
            photo.Url__c = 'http://test.com';

            photos.add(photo);
        }
        insert photos;
    }

    @isTest
    private static void getPhotosByTitle_Test() {
        String searchKey = 'TestTitle50';

        Test.startTest();
        List<Photo__c> testPhotos = ImageSearchController.getPhotosByTitle(searchKey);
        Test.stopTest();

        System.assertEquals(1, testPhotos.size());
    }

    @isTest
    private static void sendEmail_Test() {
        String searchKey = 'TestTitle50';
        String testEmail = 'imageSearchTestEmail@test.com';
        List<Photo__c> testPhotos = ImageSearchController.getPhotosByTitle(searchKey);

        Test.startTest();
        ImageSearchController.sendEmail(testPhotos, testEmail);
        Integer invocations = Limits.getEmailInvocations();
        System.assertEquals(1, invocations);
        Test.stopTest();
    }

    @isTest
    private static void sendEmail_NegativeTest() {
        String searchKey = 'TestTitle50';
        String testEmail = 'wrongEmail';
        List<Photo__c> testPhotos = ImageSearchController.getPhotosByTitle(searchKey);

        Test.startTest();
        ImageSearchController.sendEmail(testPhotos, testEmail);
        Integer invocations = Limits.getEmailInvocations();
        System.assertEquals(0, invocations);
        Test.stopTest();
    }
}