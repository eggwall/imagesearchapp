/**
 * Created by Dominika Wałęga on 20.08.2019.
 */

global class PhotoDeleteBatch implements Database.Batchable<sObject>, Database.Stateful {

    List<Photo__c> batchPhotoList = new List<Photo__c>();

    public PhotoDeleteBatch(List<Photo__c> batchPhotoList) {
        this.batchPhotoList = batchPhotoList;
    }

    global Iterable<sObject> start(Database.BatchableContext bc) {
        return shouldDelete();
    }

    global void execute(Database.BatchableContext bc, List<Photo__c> scope) {
        delete scope;
    }

    global void finish(Database.BatchableContext bc) {
    }

    public List<Photo__c> shouldDelete() {
        Set<Decimal> photoIds = new Set<Decimal>();
        for (Photo__c photo : batchPhotoList) {
            photoIds.add(photo.Id_Number__c);
        }
        List<Photo__c> recordsToDelete = [
                SELECT Id, Album_Id__c, Id_Number__c, Title__c, Url__c, Thumbnail_Url__c
                FROM Photo__c
                WHERE Id_Number__c NOT IN :photoIds
        ];
        return recordsToDelete;
    }
}
