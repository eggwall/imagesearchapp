/**
 * Created by Dominika Wałęga on 20.08.2019.
 */

public with sharing class PhotoRestCallout {

    public static List<Photo__c> getPhotosObjects() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://jsonplaceholder.typicode.com/photos');
        request.setMethod('GET');

        HttpResponse response = http.send(request);

        if (response.getStatusCode() == 200) {
            List<PhotoResponse> responseBodyList = PhotoResponse.parse(response.getBody());
            List<Photo__c> photoToUploadList = new List<Photo__c>();
            for (PhotoResponse singleObject : responseBodyList) {
                Photo__c photo = new Photo__c();
                photo.Album_Id__c = singleObject.albumId;
                photo.Id_Number__c = singleObject.id;
                photo.Title__c = singleObject.title;
                photo.Url__c = singleObject.url;
                photo.Thumbnail_Url__c = singleObject.thumbnailUrl;
                photoToUploadList.add(photo);
            }
            return photoToUploadList;
        } else {
            System.debug('Error has occurred. Status code ' + response.getStatusCode());
            return new List<Photo__c>();
        }
    }
}
