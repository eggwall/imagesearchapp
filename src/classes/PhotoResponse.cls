/**
 * Created by Dominika Wałęga on 20.08.2019.
 */

public with sharing class PhotoResponse {

    public Integer albumId;
    public Integer id;
    public String title;
    public String url;
    public String thumbnailUrl;

    public static List<PhotoResponse> parse(String json) {
        return (List<PhotoResponse>) System.JSON.deserialize(json, List<PhotoResponse>.class);
    }
}
