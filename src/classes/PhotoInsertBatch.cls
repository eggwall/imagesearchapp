/**
 * Created by Dominika Wałęga on 20.08.2019.
 */

global class PhotoInsertBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    List<Photo__c> batchPhotoList = new List<Photo__c>();

    global Iterable<sObject> start(Database.BatchableContext bc) {
        batchPhotoList = PhotoRestCallout.getPhotosObjects();
        return shouldInsert();
    }

    global void execute(Database.BatchableContext bc, List<Photo__c> scope) {
        insert scope;
    }

    global void finish(Database.BatchableContext bc) {
        PhotoUpdateBatch photoUpdateBatch = new PhotoUpdateBatch(batchPhotoList);
        Database.executeBatch(photoUpdateBatch);
    }

    public List<Photo__c> shouldInsert() {
        Set<Decimal> photoIds = new Set<Decimal>();
        for (Photo__c photo : [SELECT Id, Album_Id__c, Id_Number__c, Title__c, Url__c, Thumbnail_Url__c FROM Photo__c]) {
            photoIds.add(photo.Id_Number__c);
        }
        List<Photo__c> recordsToInsert = new List<Photo__c>();
        for (Photo__c photo : batchPhotoList) {
            if (!photoIds.contains(photo.Id_Number__c)) {
                recordsToInsert.add(photo);
            }
        }
        return recordsToInsert;
    }
}
