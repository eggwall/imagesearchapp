/**
 * Created by Dominika Wałęga on 21.08.2019.
 */

@isTest
private class PhotoRestCallout_Test {
    @isTest
    private static void getPhotosObjects_Test() {
        Test.setMock(HttpCalloutMock.class, new PhotoHttpCalloutMock());

        Test.startTest();
        List<Photo__c> testPhotos = PhotoRestCallout.getPhotosObjects();
        Test.stopTest();

        System.assertEquals(3, testPhotos.size());
    }
}