/**
 * Created by Dominika Wałęga on 21.08.2019.
 */

@isTest
private class PhotoSchedule_Test {

    @isTest
    private static void execute_Test() {
        String CRON_EXP = '0 0 0 * * ? *';

        Test.setMock(HttpCalloutMock.class, new PhotoHttpCalloutMock());

        Test.startTest();
        PhotoSchedule photoSchedule = new PhotoSchedule();
        String jobID = System.schedule('Test Photo Schedule', CRON_EXP, photoSchedule);
        Test.stopTest();
    }
}