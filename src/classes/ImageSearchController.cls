/**
 * Created by Dominika Wałęga on 21.08.2019.
 */

public with sharing class ImageSearchController {
    @AuraEnabled
    public static List<Photo__c> getPhotosByTitle(String searchKey) {
        String title = searchKey;
        List<Photo__c> photosList = [
                SELECT Album_Id__c, Id_Number__c, Thumbnail_Url__c, Title__c, Url__c
                FROM Photo__c
                WHERE Title__c LIKE :title + '%'
                ORDER BY Title__c
        ];
        return photosList;
    }

    @AuraEnabled
    public static void sendEmail(List<Photo__c> photosToSend, String emailAddress) {
        String messageBody = '';
        for (Photo__c photo : photosToSend) {
            messageBody += '<b>Id: </b>' + photo.Id + '<b> Title: </b>' + photo.Title__c + '<b> Url: </b>' + photo.Url__c + '<br/>';
        }

        try {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[]{
                    emailAddress
            };
            message.subject = 'ImageSearchApp';
            message.HTMLBody = messageBody;
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
                    message
            };
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }
}