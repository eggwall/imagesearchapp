/**
 * Created by Dominika Wałęga on 20.08.2019.
 */

global class PhotoUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    List<Photo__c> batchPhotoList = new List<Photo__c>();

    public PhotoUpdateBatch(List<Photo__c> batchPhotoList) {
        this.batchPhotoList = batchPhotoList;
    }

    global Iterable<sObject> start(Database.BatchableContext bc) {
        return shouldUpdate();
    }

    global void execute(Database.BatchableContext bc, List<Photo__c> scope) {
        update scope;
    }

    global void finish(Database.BatchableContext bc) {
        PhotoDeleteBatch photoDeleteBatch = new PhotoDeleteBatch(batchPhotoList);
        Database.executeBatch(photoDeleteBatch);
    }

    public List<Photo__c> shouldUpdate() {
        Map<Decimal, Photo__c> photoMap = new Map<Decimal, Photo__c>();
        for (Photo__c photo : batchPhotoList) {
            photoMap.put(photo.Id_Number__c, photo);
        }

        List<Photo__c> recordsToUpdate = new List<Photo__c>();
        for (Photo__c photo : [
                SELECT Id, Album_Id__c, Id_Number__c, Title__c, Url__c, Thumbnail_Url__c
                FROM Photo__c
                WHERE Id_Number__c IN :photoMap.keySet()
        ]) {
            Photo__c tempPhoto = photoMap.get(photo.Id_Number__c);
            if (isRecordChanged(photo, tempPhoto)) {
                tempPhoto.Id = photo.Id;
                recordsToUpdate.add(tempPhoto);
            }
        }
        return recordsToUpdate;
    }

    public Boolean isRecordChanged(Photo__c photo, Photo__c tempPhoto) {
        return (photo.Album_Id__c != tempPhoto.Album_Id__c ||
                photo.Id_Number__c != tempPhoto.Id_Number__c ||
                photo.Title__c != tempPhoto.Title__c ||
                photo.Url__c != tempPhoto.Url__c ||
                photo.Thumbnail_Url__c != tempPhoto.Thumbnail_Url__c);
    }
}
