/**
 * Created by Dominika Wałęga on 21.08.2019.
 */
({
    getSearchResult: function(cmp, searchKeyValue) {
        var action = cmp.get("c.getPhotosByTitle");
        action.setParams({
            "searchKey": searchKeyValue
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.photos", response.getReturnValue());
                cmp.set("v.pageNumber", 0);
                this.getDataForSinglePage(cmp);
            }
        })
        $A.enqueueAction(action);
    },

    previousPage: function(cmp) {
        var elementsPerPage = cmp.get("v.elementsPerPage");
        var data = cmp.get("v.photos");
        var pageNumber = cmp.get("v.pageNumber");
        if(pageNumber>0) {
            cmp.set("v.pageNumber", pageNumber-1);
            this.getDataForSinglePage(cmp);
        }
    },

    nextPage: function(cmp) {
        var elementsPerPage = cmp.get("v.elementsPerPage");
        var data = cmp.get("v.photos");
        var pageNumber = cmp.get("v.pageNumber");
        var dataValue = Math.ceil(data.length/elementsPerPage)-1;
        if(pageNumber<(Math.ceil(dataValue))) {
            cmp.set("v.pageNumber", pageNumber+1);
            this.getDataForSinglePage(cmp);
        }
    },

    getDataForSinglePage: function(cmp) {
        cmp.set("v.singlePageData", []);
        var elementsPerPage = cmp.get("v.elementsPerPage");
        var data = cmp.get("v.photos");
        var pageNumber = cmp.get("v.pageNumber");
        var singlePageData = cmp.get("v.singlePageData");
        for(var i=pageNumber*elementsPerPage; i<(pageNumber*elementsPerPage)+elementsPerPage; i++) {
            if(data[i] != null){
                singlePageData.push(data[i]);
            }
        }
        cmp.set("v.singlePageData", singlePageData);
    },

    sendEmail: function(cmp) {
        var data = cmp.get("v.photos");
        var userEmail = cmp.get("v.userEmail");
        var emailValidation = /\S+@\S+\.\S+/;

        if(data.length>0 && userEmail.match(emailValidation)){
        var action = cmp.get("c.sendEmail");
        action.setParams({
            "photosToSend": data,
            "emailAddress": userEmail
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                alert("Email has been send!");
            } else {
                alert("Error has occurred.");
            }
        })
        $A.enqueueAction(action);
        } else {
            alert("Error has occurred.");
        }
    }
})