/**
 * Created by Dominika Wałęga on 21.08.2019.
 */
({
    doInit: function(cmp) {
    },

    handleChange: function(cmp, event, helper) {
       var letters = /^[A-Za-z0-9 ]+$/;
        var searchKeyValue = cmp.get("v.searchKey");
        if(searchKeyValue.length > 0 && searchKeyValue.match(letters)) {
            cmp.set("v.showPhotos", true);
            helper.getSearchResult(cmp, searchKeyValue);
        } else {
            cmp.set("v.showPhotos", false);
        }
    },

    handlePreviousPage: function(cmp, event, helper) {
        helper.previousPage(cmp);
    },

    handleNextPage: function(cmp, event, helper) {
        helper.nextPage(cmp);
    },

    handleSendEmail: function(cmp, event, helper) {
        helper.sendEmail(cmp, event);
    }
})